import {
    BaseSource,
    Candidate,
    DdcOptions,
    SourceOptions,
} from "https://deno.land/x/ddc_vim@v0.17.0/types.ts#^";
import {
    Denops,
} from "https://deno.land/x/ddc_vim@v0.17.0/deps.ts#^";



type Params = {
    hyperpath: string;
};


export class Source extends BaseSource<Params> {
    async gatherCandidates(args: {
        denops: Denops,
        options: DdcOptions,
        sourceOptions: SourceOptions,
        sourceParams: Params,
        completeStr: string,
    }): Promise<Candidate[]> {

        const p = args.sourceParams as unknown as Params;
        const hyperPath = p.hyperpath;
        const files = await Deno.readDir(hyperPath);
        const fileNames = [];
        for await (const dirEntry of files) {
            if (dirEntry.isFile) {
                const filename_and_extension = dirEntry.name.split(".");
                const filename = filename_and_extension[0];

                if (filename.split('-').length <= 1) {
                    // skip index
                    continue;
                }

                const rst_completion = ":doc:`" + filename + "`";
                fileNames.push(rst_completion);
            }
        }
        const result: Candidate[] = fileNames.map((word) => ({ word }));
        return result
    }

    params(): Params {
        return {
            hyperpath: "",
        };
    }
}
